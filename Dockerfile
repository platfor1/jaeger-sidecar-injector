FROM alpine:latest
ENV PROJECT=jaeger-agent-injector
WORKDIR /usr/share/${PROJECT}

RUN mkdir -p /usr/share/${PROJECT} && \
cd /usr/share/${PROJECT}

COPY requirements.txt /usr/share/${PROJECT}/requirements.txt
COPY run.py /usr/share/${PROJECT}/run.py
COPY entrypoint.sh /usr/share/${PROJECT}/entrypoint.sh
COPY server.cert /usr/share/${PROJECT}/entrypoint.sh
COPY server.key /usr/share/${PROJECT}/entrypoint.sh

RUN apk update && \
apk upgrade && \
apk add --no-cache python3 && \
apk add --no-cache curl && \
apk add --no-cache bash && \
apk add py3-pip && \
pip3 install -r requirements.txt

ENTRYPOINT ["/bin/sh", "entrypoint.sh"]