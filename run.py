from flask import Flask,request
import logging
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


app = Flask(__name__)
app.config['DEBUG']=True

@app.route('/mutate',methods = ['POST'])
def hello():
    if request.method == 'POST':
        logger.log(logging.INFO,request.data)
        logger.log(logging.INFO, request.json)
        return "post success"

@app.route('/mutate',methods = ['GET'])
def health():
    if request.method == 'GET':
        return "success"

# if __name__ == '__main__':
#     context = ('server.cert', 'server.key')  # certificate and key files
#     app.run(host='0.0.0.0',debug=True,port=8433,ssl_context=context)